#!/bin/bash
#set -x
################################################################################
#
#   Created: 2018-12-29
#   Copyright 2019 George Crum
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#
################################################################################

### Variables ###
chef_dir=$HOME/.chef
script_name=$(basename $0)
### End Variables ###

### Functions ###
knife_phase() {
    # Set Chef Config and data bag key
    if [ "$phase" = "local" ] ; then
        chef_config="$chef_dir/local/config.rb"
        data_bag_key="$chef_dir/local/data-bag-key"
    elif [ "$phase" = "prod-ue1" ] ; then
        chef_config="$chef_dir/prod-ue1/config.rb"
        data_bag_key="$chef_dir/prod-ue1/data-bag-key"
    elif [ "$phase" = "prod-uw1" ] ; then
        chef_config="$chef_dir/prod-uw1/config.rb"
        data_bag_key="$chef_dir/prod-uw1/data-bag-key"
    elif [ "$phase" = "prod-uw2" ] ; then
        chef_config="$chef_dir/prod-uw2/config.rb"
        data_bag_key="$chef_dir/prod-uw2/data-bag-key"
    elif [ "$phase" = "qa-ue1" ] ; then
        chef_config="$chef_dir/qa-ue1/config.rb"
        data_bag_key="$chef_dir/qa-ue1/data-bag-key"
    elif [ "$phase" = "stag-ue1" ] ; then
        chef_config="$chef_dir/stag-ue1/config.rb"
        data_bag_key="$chef_dir/stag-ue1/data-bag-key"
    elif [ "$phase" = "stag-uw2" ] ; then
        chef_config="$chef_dir/stag-uw2/config.rb"
        data_bag_key="$chef_dir/stag-uw2/data-bag-key"
    else
        usage_missing_phase
        usage
    fi
}

data_bag_create() {
    echo -e "knife data bag create ${bag} ${item} --secret-file $data_bag_key -c ${chef_config}"
    knife data bag create ${bag} ${item} --secret-file $data_bag_key -c ${chef_config}
    echo ""
}

data_bag_delete() {
    echo -e "knife data bag delete ${bag} ${item} -c ${chef_config}\n"
    knife data bag delete ${bag} ${item} -c ${chef_config}
    echo ""
}

data_bag_edit() {
    echo -e "knife data bag edit ${bag} ${item} --secret-file $data_bag_key -c ${chef_config}\n"
    knife data bag edit ${bag} ${item} --secret-file $data_bag_key -c ${chef_config}
    echo ""
}

data_bag_export() {
    echo -e "knife data bag show ${bag} ${item} -F json > ${item}.json -c ${chef_config}\n"
    knife data bag show ${bag} ${item} -F json > ${item}.json -c ${chef_config}
    echo ""
}

data_bag_list_items() {
    echo -e "knife data bag show ${bag} -c ${chef_config}\n"
    knife data bag show ${bag} -c ${chef_config}
    echo ""
}

data_bag_show() {
    echo -e "knife data bag show ${bag} ${item} --secret-file $data_bag_key -c ${chef_config}\n"
    knife data bag show ${bag} ${item} --secret-file $data_bag_key -c ${chef_config}
    echo ""
}

grep_env_role() {
    echo -e "knife environment list -c ${chef_config} | grep ${grep_str}\n"
    knife environment list -c ${chef_config} | grep ${grep_str}
    echo -e "knife role list -c ${chef_config} | grep ${grep_str}\n"
    knife role list -c ${chef_config} | grep ${grep_str}
    echo ""
}

env_to_json() {
    echo -e "knife environment show ${env} -F json -c ${chef_config} > ${env}.json\n"
    knife environment show ${env} -F json -c ${chef_config} > ${env}.json
    echo ""
}

nodes_find() {
    echo -e "knife exec -c ${chef_config} -E 'nodes.find(\"chef_environment:${environment} AND role:${role}\") {|n| puts n}' | awk -F'[' '{print \$2}' | awk -F ']' '{print \$1}' | sort\n"
    eval knife exec -c ${chef_config} -E "'nodes.find(\"chef_environment:${environment} AND role:${role}\") {|n| puts n}'" | awk -F'[' '{print $2}' | awk -F ']' '{print $1}' | sort
    echo ""
}

node_show() {
    echo -e "\nknife node show ${node} -r -c ${chef_config}\n"
    knife node show ${node} -r -c ${chef_config}
    echo -e "\nknife node show ${node} -E -c ${chef_config}\n"
    knife node show ${node} -E -c ${chef_config}
    echo ""
}

node_show_detail() {
    echo -e "knife node show ${node} -F json -l -c ${chef_config} > ${node}.json\n"
    knife node show ${node} -F json -l -c ${chef_config} > ${node}.json
    echo ""
}

usage() {
cat << EOF >&2
    data_bag_create: Create data bag
        Usage: $script_name [phase] data_bag_create [bag] [item]
    data_bag_delete: Delete data bag
        Usage: $script_name [phase] data_bag_delete [bag] [item]
    data_bag_edit: Edit data bag
        Usage: $script_name [phase] data_bag_edit [bag] [item]
    data_bag_export: Export data bag to json file
        Usage: $script_name [phase] data_bag_export [bag] [item]
    data_bag_list_items: List items in data bag
        Usage: $script_name [phase] data_bag_list_items [bag]
    data_bag_show: Show data bag item contents
        Usage: $script_name [phase] data_bag_show [bag] [item]
    grep: list environments and roles matching string
        Usage: $script_name [phase] grep [string]
    env_to_json: $script_name [phase] env_to_json [environmentname]
        Usage: $script_name [phase] env_to_json [environmentname]
    node_show: Show node runlist and enviroment
        Usage: $script_name [phase] node_show [nodename]
    node_show_detail: Show node runlist and enviroment
        Usage: $script_name [phase] node_show_detail [nodename]
    nodes_find: Search for nodes using enviroment and role
        Usage: $script_name [phase] nodes_find [environment] [role]
    Typical knife commands work using normal syntax
        Usage: $script_name [phase] node edit [node_name]
EOF
    exit 1
}

usage_missing_phase() {
cat << EOF >&2
    Mising phase:
    local
    prod-ue1
    prod-uw1
    prod-uw2
    qa-ue1
    stag-ue1
    stag-uw2
EOF
}
### End Functions ###

### Main ###
phase="$1"
knife_phase
# Drop phase from args
shift;

if [[ "$1" =~ "data_bag_" ]] ; then
    func_call="$1"
    bag="$2"
    item="${3:-noitem}"
    if [ "$func_call" = "data_bag_create" ] ; then
        data_bag_create
    elif [ "$func_call" = "data_bag_delete" ] ; then
        data_bag_delete
    elif [ "$func_call" = "data_bag_edit" ] ; then
        data_bag_edit
    elif [ "$func_call" = "data_bag_export" ] ; then
        data_bag_export
    elif [ "$func_call" = "data_bag_show" ] ; then
        data_bag_show
    elif [ "$func_call" = "data_bag_list_items" ] ; then
        data_bag_list_items
    fi
elif [ "$1" = "grep" ] ; then
    if [[ -z $2 ]] ; then
        echo "Missing string to grep for."
        usage
    else
        grep_str="$2"
        grep_env_role
    fi
elif [ "$1" = "env_to_json" ] ; then
    if [[ -z $2 ]] ; then
        echo "Missing environment name."
        usage
    else
        env="$2"
        env_to_json
    fi
elif [ "$1" = "node_show" ] ; then
    if [[ -z $2 ]] ; then
        echo "Missing node."
        usage
    else
        node="$2"
        node_show
    fi
elif [ "$1" = "node_show_detail" ] ; then
    if [[ -z $2 ]] ; then
        echo "Missing node."
        usage
    else
        node="$2"
        node_show_detail "${node}"
    fi
elif [ "$1" = "nodes_find" ] ; then
    shift;
    environment="$1"
    role="$2"
    if [[ -z $environment || -z $role ]] ; then
        echo "Missing environment and/or role argument(s)."
        usage
    else
        nodes_find
    fi
else
    echo -e "knife $@ -c ${chef_config}\n"
    knife $@ -c ${chef_config}
fi
### End Main ###
