#!/usr/bin/python3
################################################################################
#
#   Created: 2019-06-25
#   Copyright 2019 George Crum
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#
################################################################################

### Python Modules ###
import os
import re
import subprocess
import sys
import threading

from pygments import highlight
from pygments.lexers import PythonLexer
from pygments.formatters import get_all_formatters
from pygments.lexers import guess_lexer, guess_lexer_for_filename
### End Python Modules ###

### Variables ###
chef_dir = "{}/.chef".format(os.environ["HOME"])
script_name = os.path.basename(__file__)
phase = sys.argv[1]
command = sys.argv[2]
arg_list = ' '.join(sys.argv[2:])
### End Variables ### 

### Functions ###
def usage():
    print (
    '''
    data_bag_create: Create data bag
        Usage: $script_name [phase] data_bag_create [bag] [item]
    data_bag_delete: Delete data bag
        Usage: $script_name [phase] data_bag_delete [bag] [item]
    data_bag_edit: Edit data bag
        Usage: $script_name [phase] data_bag_edit [bag] [item]
    data_bag_export: Export data bag
        Usage: $script_name [phase] data_bag_export [bag] [item]
    data_bag_list_items: List items in data bag
        Usage: $script_name [phase] data_bag_list_items [bag] [item]
    data_bag_show: Show data bag item contents
        Usage: $script_name [phase] data_bag_show [bag] [item]
    grep: list environments and roles matching string
        Usage: $script_name [phase] grep [string]
    env_to_json: $script_name [phase] [environmentname]
        Usage $script_name [phase] [environmentname]
    node_show: Show node runlist and enviroment
        Usage: $script_name [phase] node_show [nodename]
    node_show_detail: Output detail node info to a json file
        Usage: $script_name [phase] node_show_detail [nodename]
    nodes_find: Search for nodes using enviroment and role")
        Usage: $script_name [phase] nodes_find -e [environment] -r [role]
    Typical knife commands work using normal syntax
        Usage: $script_name [phase] node edit [node_name]
    '''
    )
    sys.exit(1)

def usage_missing_phase():
    print (
    '''
    Mising phase:
    local
    prod-ue1
    prod-uw1
    prod-uw2
    qa-ue1
    stag-ue1
    stag-uw2
    '''
    )
    sys.exit(1)

def data_bag_create():
    cmd = "knife data bag create {0} {1} --secret-file {2} -c {3}".format(bag, item, data_bag_key, chef_config)
    print()
    print(cmd, end='\n\n')
    subprocess.call(cmd, shell=True)

def data_bag_delete():
    cmd = "knife data bag delete {0} {1} -c {2}".format(bag, item, chef_config)
    print()
    print(cmd, end='\n\n')
    subprocess.call(cmd, shell=True)

def data_bag_edit():
    cmd = "knife data bag edit {0} {1} --secret-file {2} -c {3}".format(bag, item, data_bag_key, chef_config)
    print()
    print(cmd, end='\n\n')
    subprocess.call(cmd, shell=True)

def data_bag_export():
    cmd = "knife data bag show {0} {1} -F json > {0}.json -c {2}".format(bag, item, chef_config)
    print()
    print(cmd, end='\n\n')
    subprocess.call(cmd, shell=True)
    print()

def data_bag_list_items():
    cmd = "knife data bag show {0} -c {1}".format(bag, chef_config)
    print()
    print(cmd, end='\n\n')
    subprocess.call(cmd, shell=True)
    print()

def data_bag_show():
    cmd = "knife data bag show {0} {1} --secret-file {2} -c {3}".format(bag, item, data_bag_key, chef_config)
    print()
    print(cmd, end='\n\n')
    subprocess.call(cmd, shell=True)

def grep_env_role():
    cmd1 = "knife environment list -c {0} | grep {1}".format(chef_config, grep_str)
    cmd2 = "knife role list -c {0} | grep {1}".format(chef_config, grep_str)
    print()
    print(cmd1, end='\n\n')
    subprocess.call(cmd1, shell=True)
    print()
    print(cmd2, end='\n\n')
    subprocess.call(cmd2, shell=True)
    print()

def env_to_json():
    cmd = "knife environment show {0} -F json -c {1} > {0}.json".format(env, chef_config)
    print()
    print(cmd, end='\n\n')
    subprocess.call(cmd, shell=True)
    print()

def node_show():
    cmd1 = "knife node show {0} -r -c {1}".format(node, chef_config)
    cmd2 = "knife node show {0} -E -c {1}".format(node, chef_config)
    print()
    print(cmd1, end='\n\n')
    subprocess.call(cmd1, shell=True)
    print()
    print(cmd2, end='\n\n')
    subprocess.call(cmd2, shell=True)
    print()

def node_show_environment(node):
    global node_env
    getenv = "knife node show {0} -E -c {1}".format(node, chef_config)
    print()
    print(getenv, end='\n')
    node_env = subprocess.run(getenv, shell=True, stdout=subprocess.PIPE)

def node_show_role(node):
    global node_role
    getrole = "knife node show {0} -r -c {1}".format(node, chef_config)
    print(getrole, end='\n\n')
    node_role = subprocess.run(getrole, shell=True, stdout=subprocess.PIPE)

def node_show_detail():
    cmd = "knife node show {0} -F json -l -c {1} > {0}.json".format(node, chef_config)
    print()
    print(cmd, end='\n\n')
    subprocess.call(cmd, shell=True)
    print()

def nodes_find():
    cmd = "knife exec -c {0} -E 'nodes.find(\"chef_environment:{1} AND role:{2}\") {{|n| puts n}}' | awk -F\'[\' \'{{print $2}}\' | awk -F \']\' \'{{print $1}}\' | sort".format(chef_config, environment, role)
    print()
    print(cmd, end='\n\n')
    subprocess.call(cmd, shell=True)
    print()
### End Functions ###

### Main ###
# Get phase and set chef specific variables
phase = sys.argv[1]
if phase == 'local':
    chef_config = "{}/local/config.rb".format(chef_dir)
    data_bag_key = "{}/local/data-bag-key".format(chef_dir)
elif phase == 'prod-ue1':
    chef_config = "{}/prod-ue1/config.rb".format(chef_dir)
    data_bag_key = "{}/prod-ue1/data-bag-key".format(chef_dir)
elif phase == 'prod-uw1':
    chef_config = "{}/prod-uw1/config.rb".format(chef_dir)
    data_bag_key = "{}/prod-uw1/data-bag-key".format(chef_dir)
elif phase == 'prod-uw2':
    chef_config = "{}/prod-uw2/config.rb".format(chef_dir)
    data_bag_key = "{}/prod-uw2/data-bag-key".format(chef_dir)
elif phase == 'qa-ue1':
    chef_config = "{}/qa-ue1/config.rb".format(chef_dir)
    data_bag_key = "{}/qa-ue1/data-bag-key".format(chef_dir)
elif phase == 'stag-ue1':
    chef_config = "{}/stag-ue1/config.rb".format(chef_dir)
    data_bag_key = "{}/stag-ue1/data-bag-key".format(chef_dir)
elif phase == 'stag-uw2':
    chef_config = "{}/stag-uw2/config.rb".format(chef_dir)
    data_bag_key = "{}/stag-uw2/data-bag-key".format(chef_dir)
else:
    usage_missing_phase()
    usage()

# Evaluate command and call specific function
bag_it = re.search(r'(^)data(.*)', command)
if bag_it:    
    bag = sys.argv[3]
    if len(sys.argv) == 5:
        item = sys.argv[4]
    else:
        item = "noitem"

    if  command == 'data_bag_create':
        data_bag_create()
    elif command == 'data_bag_delete':
        data_bag_delete()
    elif command == 'data_bag_edit':
        data_bag_edit()
    elif command == 'data_bag_export':
        data_bag_export()
    elif command == 'data_bag_show':
        data_bag_show()
    elif command == 'data_bag_list_items':
        data_bag_list_items()
elif command == 'env_to_json':
    env = sys.argv[3]
    env_to_json()
elif command == 'node_show':
    node = sys.argv[3]
    t1 = threading.Thread(target=node_show_environment, args=(node,))
    t2 = threading.Thread(target=node_show_role, args=(node,))
    t1.start()
    t2.start()
    t1.join()
    t2.join()
    print(node_role.stdout.decode())
    print(node_env.stdout.decode())
elif command == 'node_show_detail':
    node = sys.argv[3]
    node_show_detail()
elif command == 'nodes_find':
    environment = sys.argv[3]
    role = sys.argv[4]
    nodes_find()
elif command == 'grep':
    grep_str = sys.argv[3]
    grep_env_role()
else:
    cmd = "knife {0} -c {1}".format(arg_list, chef_config)
    print()
    print(cmd)
    print()
    subprocess.call(cmd, shell=True)
    print()
### End Main ###
