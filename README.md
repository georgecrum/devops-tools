# devops-tools

    Simple scripts that make my job a little easier.
        Originally knifewrapper.sh was intended to just manage multiple chef servers.
        But it became what it is now.  Decided to make a python version of knife-wrapper.sh
        script to hone my python skill.  Doing this helped me recognize some improvements 
        to the bash version.

## knife-wrapper.sh and knife-wrapper.py
    Wrapper for knife a chef workstation tool.
        - Handle multiple knife config.rb files for multiple chef shop.
        - List out environments and roles matching specified string.
        - Display node runlist and envronment.
        - Simplified node search.
        - Create, edit, export, list or show encrypted data bags.
        - Just use plain knife syntax.
        - Prints out knife command before executing.

## sshcmd.exp
    Script will run commands on list of servers.
        - Choose to drop in to interactive mode
        - Prompt for user password one time and use for logging in and sudo

## sshsu.exp
    Script to ssh to server and sudo to root then drop to interactive prompt.

## shaping-room
    Snippets of code used for testing portions of larger scripts.
