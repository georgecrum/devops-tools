#!/usr/bin/expect -f

################################################################################
#
#   Copyright 2019 George Crum
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Created: 2008
#   Description: SSH to server and sudo to root prompting for password once and
#     drop to interactive mode.
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#
################################################################################

#exp_internal 1
#strace 4

### Variables ###
set server [lrange $argv 0 0]
set prompt "(%|\\\$|#|\\\?) "
set userpass ""
set username "[exec whoami]"
set sudoprompt "(Password: |Password:|\\\[sudo] password for $username: )$"
### End Variables ###

# Check that hostname resolves in DNS
log_user 0
spawn -noecho dig $server
expect {
    "status: SERVFAIL" {
    send_error "Server name: $server\n"
    send_error "Does not resolve in DNS.\n"
    exit 1
    } "ANSWER: 0" {
    send_error "Server name: $server\n"
    send_error "Does not resolve in DNS.\n"
    exit 1
    }
}

# Turn off terminal echo and get password from user
set timeout 60
log_user 1
send_user "The password for $server: "
stty -echo
expect_user -re "(.*)\n" {set userpass $expect_out(1,string)}
send_user "\n"
stty echo
if 0==[llength $userpass] {
    send_error "No Null Passwords\n"
    exit 1
}

# Set indefinite timeout due to server slow response
set timeout -1

# Start ssh session and handle new host keys if needed.
spawn ssh $server
# Make sure expect has our window size (SIGWINCH) to present to applications like vim.
trap {
  set rows [stty rows]
  set cols [stty columns]
  stty rows $rows columns $cols < $spawn_out(slave,name)
} WINCH
expect {
"The authenticity of host '*' can't be established.\r
* key fingerprint is *.\r
Are you sure you want to continue connecting (yes/no)? " {
	send -- "yes\r"
	expect "yes\r
Warning: Permanently added '*' (RSA) to the list of known hosts.\r"
    expect "password:" { send "$userpass\n"}
    expect -re $prompt
    send "sudo -k\n"
    expect -re $prompt
    send "sudo su -\n"
    expect -re "$sudoprompt" { send "$userpass\n"}
    log_user 1
    expect -re $prompt
    send "\r"
    interact
} "password:" { send "$userpass\n"
    expect -re $prompt
    send "sudo -k\n"
    expect -re $prompt
    send "sudo su -\n"
    expect -re "$sudoprompt" { send "$userpass\n"}
    log_user 1
    expect -re $prompt
    send "\r"
    interact
} -re $prompt {
    send "sudo -k\n"
    expect -re $prompt
    send "sudo su -\n"
    expect -re "$sudoprompt" { send "$userpass\n"}
    log_user 1
    expect -re $prompt
    send "\r"
    interact
} }
