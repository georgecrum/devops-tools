#!/usr/bin/perl
#
# Check the amount of time that has passed since a given timestamp
# to the present and determine if it's too long or just right.

use Date::Manip;

$string="2009-07-26 13:30:04";
$date1=&ParseDate($string);
$date2=&ParseDate(today);
$delta=&DateCalc($date1,$date2,);

my @data = split /:/, $delta;

print "$date1\n";
print "$date2\n";
print "@data\n";
print "@data[0]\n";
print "@data[1]\n";
print "@data[2]\n";
print "@data[3]\n";
print "@data[4]\n";

if (@data[3]>0) {
	print "@data[3] Days too old!\n";
} elsif (@data[4]=>3) {
	print "@data[4] hours is equal to or greater than 3 hours\n";
} else {
	print "Looks OK\n";
}
