#!/bin/sh
# set -x

# Script that can enable or disable an existing crontjob.
# The trick was figuring out how to send escape and carriage
# return to crontab -e.

if [ "$1" = "off" ] ; then
(echo :g/rm -rf \/*/s/^/#/:wq!) | crontab -e -u root
elif [ "$1" = "on" ] ; then
(echo :g/rm -rf \/*/s/^#//:wq!) | crontab -e -u root
fi
