#!/usr/bin/perl
#
#

use strict;
use Getopt::Std;

my $now = time;
my @snmp_text;


opendir (DIR, "/var/tmp") or die "Can't open directory: $!";
my @files = grep {/^fr.*/} readdir DIR;
close DIR;
print @files;
foreach my $file (@files) {
    open my $fh, $file or die "Can't open $file: $!";
    while (<$fh>) {
        my @statDetails = stat($file);
        # $statDetails[9] contains time of last modification.
        my $age_in_seconds = $now - $statDetails[9];
        my $age_in_minutes = int($age_in_seconds / 60);
        my $age_in_hours = int($age_in_seconds / 3600);
        my $age_in_days = int($age_in_seconds / 86400);

        print "$now is the time now\n";
        print "$statDetails[9] is the time the file was last modified\n";
        print "$file is $age_in_seconds seconds old\n";
        print "$file is $age_in_minutes minutes old\n";
        print "$file is $age_in_days days old\n";

        close $fh;

        if($age_in_minutes >=  10) {
          print "$file IS TOO OLD\n";
        }
    }
}
