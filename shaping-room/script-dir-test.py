#!/usr/bin/env python3

# Check if the script is being run from the same directory
# as the script.

import os.path

script_dir = os.path.split(os.path.abspath(__file__))[0]
print(script_dir)
cwd = os.getcwd()
print(cwd)
if script_dir == cwd:
  print("You are in the correct directory")
else:
  print("change to script dir")
