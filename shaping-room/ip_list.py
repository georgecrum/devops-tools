#!/usr/bin/env python3

# Import variable from base_list file.
# Print list of IP addresses from base_list.

import ipaddress
import os.path
import sys
import base_list

first_ip = base_list.first_ip
last_ip = base_list.last_ip
ip_list = []

for ip in range(int(ipaddress.IPv4Address(first_ip)),
                    int(ipaddress.IPv4Address(last_ip))+1):
    ip_list.append(format(ipaddress.IPv4Address(ip)))
    print(ipaddress.IPv4Address(ip))
print(ip_list)
for a in ip_list:
  print(a)
